﻿using UnityEngine;
using System.Collections;
using System;

public class Makkara : MonoBehaviour, IAction
{

    public float grillausAika;
    public Material kypsaMakkara;
    private bool kypsa = false;
    private bool kypsyy = false;
    private float grillattu = 0;

    public void doAction()
    {
        if (kypsa)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Pisteet>().lisaaPisteet(10);
            Destroy(gameObject);
        }
        Debug.Log(kypsa);

    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (kypsyy)
        {
            if(grillattu >= grillausAika)
            {
                kypsa = true;
                kypsyy = false;
                GetComponent<Renderer>().material = kypsaMakkara;
                Debug.Log(kypsa);

            }
            grillattu += Time.deltaTime;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Grilli" && !kypsa)
        {
            kypsyy = true;
            collision.gameObject.GetComponent<AudioSource>().Play();
        }
    }

    void OnCollisionExit(Collision collision)
    {
        kypsyy = false;
    }


}