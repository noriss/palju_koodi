﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pisteet : MonoBehaviour {

    public GameObject pisteTeksti;
    public GameObject radio;
    public GameObject pisteLisäys;

    private int pisteet = 0;



    public void lisaaPisteet(int pisteet)
    {

        if (radio.GetComponent<Radio>().Paalla)
        {
            int temp = (int)(pisteet * 1.5);
            Debug.Log(temp);
            if (GetComponent<Humalatila>().getHumala())
            {
                temp  = (int)(temp * 1.5);

            }

            this.pisteet += temp;
            string teksti = "Score: ";
            teksti += this.pisteet;
            pisteTeksti.GetComponent<Text>().text = teksti;
        }
        else if (GetComponent<Humalatila>().getHumala())
        {
            int temp = (int)(pisteet * 1.5);
            string teksti = "Score: ";
            this.pisteet += temp;
            teksti += this.pisteet;
            pisteTeksti.GetComponent<Text>().text = teksti;
            StartCoroutine(pisteSaanti(pisteet));
            Debug.Log(temp);
        }
        else
        {
            string teksti = "Score: ";
            this.pisteet += pisteet;
            teksti += this.pisteet;
            pisteTeksti.GetComponent<Text>().text = teksti;
            StartCoroutine(pisteSaanti(pisteet));

        }
        Debug.Log(this.pisteet);
    }

    IEnumerator pisteSaanti(float maara)
    {

        pisteLisäys.GetComponent<Text>().text = "+" + maara;
        fadeIn();
        yield return new WaitForSeconds(6);
        pisteLisäys.GetComponent<Text>().text = "";
        FadeOut();
    }

    private float startAlpha = 0;
    private float endAlpha = 1;
    private float timeSoFar = 0;
    private bool fading = true;
    private float changeTimeSecounds = 3;
    private float changeRate;

    void fadeIn()
    {
        startAlpha = 0;
        endAlpha = 1;
        timeSoFar = 0;
        StartCoroutine(FadeCoroutine());
    }
    public void FadeOut()
    {
        startAlpha = 1;
        endAlpha = 0;
        timeSoFar = 0;
        fading = true;
        StartCoroutine(FadeCoroutine());
    }

    IEnumerator FadeCoroutine()
    {
        changeRate = (endAlpha - startAlpha) / changeTimeSecounds;
        setAlpha(startAlpha);

        while (fading)
        {
            timeSoFar += Time.deltaTime;
            if (timeSoFar > changeTimeSecounds)
            {
                fading = false;
                setAlpha(endAlpha);
                yield break;

            }
            else
                setAlpha(pisteLisäys.GetComponent<Text>().color.a + (changeRate * Time.deltaTime));
        }
    }

    private void setAlpha(float alpha)
    {
        Color vari = pisteLisäys.GetComponent<Text>().color;
        vari.a = Mathf.Clamp(alpha, 0, 1);
        pisteLisäys.GetComponent<Text>().color = vari;
    }

    public int getScore()
    {
        return this.pisteet;
    }
}
