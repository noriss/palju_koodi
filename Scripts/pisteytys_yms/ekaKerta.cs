﻿using UnityEngine;
using System.Collections;

public class ekaKerta : MonoBehaviour {

    public Scores[] alkuPisteet = new Scores[10];
	// Use this for initialization
	void Start () {
        HighScore score = HighScore._instance;

        if (!PlayerPrefs.HasKey("FirstTime"))
        {
            PlayerPrefs.SetInt("FirstTime", 0);
        }
        if (PlayerPrefs.HasKey("FirstTime"))
        {
            if(PlayerPrefs.GetInt("FirstTime") != 1)
            {
                for(int i = 0; i < alkuPisteet.Length; i++)
                {
                    score.SaveHighScore(alkuPisteet[i].name, alkuPisteet[i].score);
                }
                PlayerPrefs.SetInt("FirstTime", 1);
                PlayerPrefs.Save();
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
