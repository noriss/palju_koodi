﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class pisteidenTallennus : MonoBehaviour {

    public List<GameObject> tilat = new List<GameObject>(10);
	// Use this for initialization
	void Start () {
        HighScore score = HighScore._instance;
        List<Scores> pisteet = score.GetHighScore();


        for (int i = 0; i < tilat.Count; i++)
        {
            string scoreTeksti = pisteet[i].name + " " + pisteet[i].score;
            tilat[i].GetComponent<Text>().text = scoreTeksti;
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
