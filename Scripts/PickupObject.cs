﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class PickupObject : MonoBehaviour {

    GameObject mainCamera;
    GameObject pickedUpObject;
    bool pickedUp;
    public float distance;
    public float smoothness;
    public bool colliding = false;
    public Vector3 turnDirection = new Vector3();
    public KeyCode toimintaNappi = KeyCode.R;
    private bool rotationg = false;
    private Vector3 actionPoint;


	void Start () {
        mainCamera = GameObject.FindWithTag("MainCamera");
        actionPoint = new Vector3(Screen.width / 2, Screen.height / 2);
	}

    void FixedUpdate() {
        if (pickedUp && pickedUpObject != null) {
            carry();
            if(!pickedUpObject.tag.Contains("Kauha"))
                rotateAround();
            checkAction();
            checkDrop();
        }
        else {

            pickUp();
            tarkistaAction();
            
        }
    }

    private void tarkistaAction()
    {
        if (Input.GetKeyDown(toimintaNappi))
        {

            if (Time.time - lastTime >= 1f)
            {
                Ray pickupRay = mainCamera.GetComponent<Camera>().ScreenPointToRay(actionPoint);
                RaycastHit pickupRayHit;
                if (Physics.Raycast(pickupRay, out pickupRayHit))
                {
                    IAction toiminto = pickupRayHit.collider.GetComponent(typeof(IAction)) as IAction;
                    toiminto.doAction();
                }
                lastTime = Time.time;
            }
        }

    }

    private float lastTime = 0;

    private void checkAction()
    {
        if (Time.time - lastTime >= 1f)
        {
            if (pickedUpObject.gameObject.name.Contains("tolkki"))
            {
                IAction toiminto = pickedUpObject.GetComponent(typeof(IAction)) as IAction;
                toiminto.doAction();
            }
            if (Input.GetKeyDown(toimintaNappi))
            {
                IAction toiminto = pickedUpObject.GetComponent(typeof(IAction)) as IAction;
                toiminto.doAction();
                
            }
            if (pickedUpObject.gameObject.name.Contains("kauha"))
            {
                pickedUpObject.GetComponent<Kauha>().doAction();
            }
            lastTime = Time.time;
        }
    }

    private void carry() {
        if(pickedUpObject == null)
        {
            pickedUp = false;
            return;
        }
        pickedUpObject.transform.position = Vector3.Lerp(pickedUpObject.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smoothness);
       
    }

    private void rotateAround()
    {
        if (Input.GetMouseButton(0))
        {

            GetComponent<FirstPersonController>().enabled = false;
            rotationg = true;
            
        }

        if (rotationg)
        {
            float rotationx = Input.GetAxis("Mouse X");
            float yRot = Input.GetAxis("Mouse Y");


            pickedUpObject.transform.Rotate(new Vector3(0, 1, 0), rotationx);
            pickedUpObject.transform.Rotate(new Vector3(1, 0, 0), yRot);
        }

        if (Input.GetMouseButtonUp(0))
        {
            GetComponent<FirstPersonController>().enabled = true;
            rotationg = false;
        }
    }
	
	private void pickUp() {
        if (Input.GetKeyDown(KeyCode.E)) {


            Ray pickupRay = mainCamera.GetComponent<Camera>().ScreenPointToRay(actionPoint);
            RaycastHit pickupRayHit;
            if (Physics.Raycast(pickupRay, out pickupRayHit)) {
                GameObject pi = pickupRayHit.collider.gameObject;
                Debug.Log(pi.name);
                Pickupable p = pickupRayHit.collider.GetComponent<Pickupable>();
                if (p != null)
                {
                    pickedUp = true;
                    pickedUpObject = p.gameObject;

                    p.gameObject.GetComponent<Rigidbody>().useGravity = false;
                    pickedUpObject.GetComponent<Pickupable>().pickedUp = true;
                    pickedUpObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                    pickedUpObject.transform.parent = transform;

                }
                
            }
        }
	}

    public void pickupItem(GameObject tavara)
    {
        pickedUp = true;
        pickedUpObject = tavara;

        pickedUpObject.GetComponent<Rigidbody>().useGravity = false;
        pickedUpObject.GetComponent<Pickupable>().pickedUp = true;
        pickedUpObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        pickedUpObject.transform.parent = transform;
    }

	private void checkDrop() {
		if (Input.GetKeyDown(KeyCode.E)) {
			dropObject();
		}
	}

	private void dropObject() {
		pickedUp = false;
		//pickedUpObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
		pickedUpObject.gameObject.GetComponent<Rigidbody>().useGravity = true;
        pickedUpObject.GetComponent<Pickupable>().pickedUp = false;
        pickedUpObject.transform.parent = null;
        pickedUpObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		pickedUpObject = null;
	}



}
