﻿using UnityEngine;
using System.Collections;
using System;

public interface IAction
{
    void doAction();

}

public class Kalja : MonoBehaviour, IAction {

    public GameObject pelaaja;
    public GameObject juoParent;
    public AnimationClip juontu;
    public AudioClip juontiAani;

    public void doAction()
    {
        transform.parent = juoParent.transform;
        pelaaja.GetComponent<Humalatila>().juoOlut();
        GetComponent<AudioSource>().clip = juontiAani;
        GetComponent<AudioSource>().Play();
        GetComponent<Animator>().SetTrigger("Juo");
        pelaaja.GetComponent<Pisteet>().lisaaPisteet(4);
        StartCoroutine(juo());
    }



    IEnumerator juo()
    {
        yield return new WaitForSeconds(juontu.length);

        Destroy(this.gameObject);
    }
}
