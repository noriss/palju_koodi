﻿using UnityEngine;
using System.Collections;
using System;

public class alotaVedenNosto : MonoBehaviour, IAction {

    public GameObject vesi;
    public GameObject partikkeli;

    public void doAction()
    {
        vesi.GetComponent<NostaVesi>().aloitaVedenNosto();
        GetComponent<AudioSource>().Play();
        partikkeli.SetActive(true);
    }

}
