﻿using UnityEngine;
using System.Collections;
using System;

public class NostaVesi : MonoBehaviour {

    public Transform ylaosa;

    [Range(1, 60)]
    public int nostoNopeus = 10;
    public GameObject kahva;
    public GameObject partikkeli;
    

    public bool nostoAlotettu = false;
    private bool nostettu = false;
    private Vector3 velocity;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (nostoAlotettu)
        {

            transform.position = Vector3.SmoothDamp(transform.position, ylaosa.position, ref velocity, nostoNopeus);

        }

        if(Vector3.Distance(transform.position, ylaosa.position) <= 0.1f)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Pisteet>().lisaaPisteet(100);
            GetComponent<NostaVesi>().enabled = false;
            kahva.GetComponent<AudioSource>().Stop();
            partikkeli.SetActive(false);
            nostettu = true;
        }
	}

    public void aloitaVedenNosto()
    {
        nostoAlotettu = true;
        Debug.Log("Alotattu");
    }

    public bool onkoNostettu()
    {
        return nostettu;
    }
}
