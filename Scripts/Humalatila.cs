﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;

public class Humalatila : MonoBehaviour {

    public int toleranssi = 6;
    public float vahennysAika = 30f;
    public int maara;
    private int juotumaara = 0;
    private bool humalaEfekti = false;


	// Use this for initialization
	void Start () {
        StartCoroutine(vahennaHumalaa());
	}
	
	// Update is called once per frame
	void Update () {
        if (juotumaara <= toleranssi && humalaEfekti)
        {
            GetComponentInChildren<MotionBlur>().enabled = false;
            GetComponentInChildren<TiltShift>().enabled = false;
            GetComponentInChildren<Fisheye>().enabled = false;
        }
    }

    public void juoOlut()
    {
        maara++;
        juotumaara++;
        
        if(juotumaara >= toleranssi)
        {
            GetComponentInChildren<MotionBlur>().enabled = true;
            GetComponentInChildren<TiltShift>().enabled = true;
            GetComponentInChildren<Fisheye>().enabled = true;
            humalaEfekti = true;
        }
    }

    public bool getHumala()
    {
        return humalaEfekti;
    }

    IEnumerator vahennaHumalaa()
    {
        while (true)
        {
            yield return new WaitForSeconds(vahennysAika);
            juotumaara--;
            maara--;

        }
    }
}
