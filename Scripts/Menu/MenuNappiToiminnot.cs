﻿using UnityEngine;
using System.Collections;

public class MenuNappiToiminnot : MonoBehaviour {

    public GameObject startCamera;
    public GameObject pelaaja;
    public GameObject HighscorePaneeli;
    public GameObject howtoplayPaneeli;
    public GameObject gameInfo;
    public GameObject menu;
    public GameObject options;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void startGame()
    {

        StartCoroutine(alotusEfecti());
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void HighScore()
    {
        HighscorePaneeli.SetActive(true);
        menu.SetActive(false);
    }

    public void HowtoPlay()
    {
        howtoplayPaneeli.SetActive(true);
        menu.SetActive(false);
    }

    public void Options()
    {
        options.SetActive(true);
        menu.SetActive(false);
    }


    IEnumerator alotusEfecti()
    {
        
        startCamera.GetComponent<CameraFade>().startAlpha = 0;
        startCamera.GetComponent<CameraFade>().FadeOut(3, 1);
        yield return new WaitForSeconds(3);
        menu.SetActive(false);
        gameObject.SetActive(false);
        gameInfo.SetActive(true);
        startCamera.SetActive(false);
        pelaaja.SetActive(true);
        gameObject.SetActive(true);
        Debug.Log(pelaaja.activeSelf);
    }
}
