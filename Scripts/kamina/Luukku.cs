﻿using UnityEngine;
using System.Collections;
using System;

public class Luukku : MonoBehaviour, IAction {

    private bool avattu = false;

    public AnimationClip avaa, sulje;
    public AudioClip avaaAudio, suljeAudio;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void doAction()
    {

        if (!avattu)
        {
            GetComponent<Animator>().Rebind();
            GetComponent<Animator>().SetTrigger("Avaa");
            avattu = !avattu;
            GetComponent<AudioSource>().clip = avaaAudio;
            GetComponent<AudioSource>().Play();
            StartCoroutine(doAnimation(1f));
        }
        else
        {
            GetComponent<Animator>().Rebind();
            GetComponent<Animator>().SetTrigger("Sulje");
            avattu = !avattu;
            GetComponent<AudioSource>().clip = suljeAudio;
            GetComponent<AudioSource>().Play();
            StartCoroutine(SuljeAnimaatio(1f));
        }



    }

    IEnumerator doAnimation(float aika)
    {
        yield return new WaitForSeconds(aika);
        GetComponent<Animator>().ResetTrigger("Avaa");
    }

    IEnumerator SuljeAnimaatio(float aika)
    {
        yield return new WaitForSeconds(aika);
        GetComponent<Animator>().ResetTrigger("Sulje");
    }
}
