﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PaljuTeksti : MonoBehaviour {

    public GameObject uiPalju;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        string teksti = "Palju: ";
        teksti += (GetComponent<KaminaToiminta>().lampotila / GetComponent<KaminaToiminta>().targetLampo) * 100 + "%";
        uiPalju.GetComponent<Text>().text = teksti;
    }
}
