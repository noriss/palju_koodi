﻿using UnityEngine;
using System.Collections;

public class SaunaTarkastus : MonoBehaviour {

    public GameObject globalCheck;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<KaminaToiminta>().onkoValmis())
        {
            globalCheck.GetComponent<peliTarkistus>().saunaPaalla = true;
        }
        
	}
}
