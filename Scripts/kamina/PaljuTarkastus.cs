﻿using UnityEngine;
using System.Collections;

public class PaljuTarkastus : MonoBehaviour {

    public GameObject globalCheck;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<KaminaToiminta>().onkoValmis())
        {
            globalCheck.GetComponent<peliTarkistus>().palju = true;
        }

    }
}
