﻿using UnityEngine;
using System.Collections;

public class KaminaToiminta : MonoBehaviour {

    public float tasausAika = 5f;
    public float laskujaksoAika = 1f;
    public float laskuMaara = 1f;
    public int pisteSaanti;
    public float minSaunaLampo;
    public float targetLampo;
    public float nostoMaara;
    private int puuMaara = 0;
    public bool luukkuAuki = false;
    public int palamisAika;
    public GameObject pelaaja;
    public AudioClip[] puuAanet;
    public AudioClip tuliAani;
    public AudioSource tuli;
    public AudioSource puu;


    private bool ekaPuu = true;

    public float lampotila = 21;
    private float edellinenAika = 0;
    private bool lasku = false;
    private bool Valmis = false;

	// Use this for initialization
	void Start () {
        tuli.clip = tuliAani;
        tuli.loop = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (lasku)
        {
            if(lampotila >= targetLampo)
            {
                lasku = false;
                pelaaja.GetComponent<Pisteet>().lisaaPisteet(pisteSaanti);
                lampotila = targetLampo;
                Valmis = true;
            }
            if(Time.time - edellinenAika >= laskujaksoAika && lampotila > minSaunaLampo)
            {
                lampotila -= laskuMaara;

                edellinenAika = Time.time;
            }
        }
	}

    public void lisaaPuu()
    {
        if (!Valmis)
        {
            if (ekaPuu)
            {
                pelaaja.GetComponent<Pisteet>().lisaaPisteet(100);
                ekaPuu = false;
                tuli.Play();

            }
            puuMaara++;
            StartCoroutine(puuPalaa());
            puu.clip = puuAanet[Random.Range(0, puuAanet.Length)];
            puu.Play();
            lampotila += nostoMaara;
            lasku = false;
            StartCoroutine(pitoOdotus());
        }
    }

    public int heuPuuMaara()
    {
        return puuMaara;
    }

    public void luukku(bool auki)
    {
        if (auki)
        {
            tuli.Play();
        }else
        {
            tuli.Stop();
        }
    }

    IEnumerator puuPalaa()
    {
        yield return new WaitForSeconds(palamisAika);
        puuMaara--;
    }

    IEnumerator pitoOdotus()
    {
        yield return new WaitForSeconds(tasausAika);
        if (puuMaara <= 0)
        {
            lasku = true;
        }
    }

    
    public bool onkoValmis()
    {
        return Valmis;
    }

}
