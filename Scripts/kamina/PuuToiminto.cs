﻿using UnityEngine;
using System.Collections;

public class PuuToiminto : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PolttoAlue")
        {
            if (other.transform.parent.GetComponentInChildren<KaminaToiminta>().heuPuuMaara() < 4)
            {
                other.transform.parent.GetComponentInChildren<KaminaToiminta>().lisaaPuu();
                Destroy(this.gameObject);
            }
        }
    }
}
