﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class saunaTeksti : MonoBehaviour {

    public GameObject uiSauna;
	
	// Update is called once per frame
	void Update () {
        string teksti = "Sauna: ";
        teksti += (GetComponent<KaminaToiminta>().lampotila / GetComponent<KaminaToiminta>().targetLampo) * 100 + "%";
        uiSauna.GetComponent<Text>().text = teksti;
	}
}
