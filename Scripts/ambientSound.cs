﻿using UnityEngine;
using System.Collections;

public class ambientSound : MonoBehaviour {

    public AudioClip ulkoMusiikki;
    public bool sisalla = false;
    
    public AudioSource musiikkiLahde;
	// Use this for initialization
	void Start () {
        musiikkiLahde.clip = ulkoMusiikki;
        musiikkiLahde.Play();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void siirto(bool sisalle)
    {
        if (sisalle)
        {
            musiikkiLahde.Stop();
        }else
        {
            musiikkiLahde.Play();
        }
    }
}
