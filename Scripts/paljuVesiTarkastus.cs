﻿using UnityEngine;
using System.Collections;

public class paljuVesiTarkastus : MonoBehaviour {

    public GameObject vesi;

	// Use this for initialization
	void Start () {
        GetComponent<KaminaToiminta>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!vesi.GetComponent<NostaVesi>().onkoNostettu())
        {
            GetComponent<KaminaToiminta>().enabled = true;
        }
	}
}
