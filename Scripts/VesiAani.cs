﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VesiAani : MonoBehaviour {

    public List<AudioClip> sisaan = new List<AudioClip>();
    public List<AudioClip> ulos = new List<AudioClip>();
    public AudioSource ulosMeno;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        ulosMeno.clip = sisaan[Random.Range(0, sisaan.Count)];
        ulosMeno.Play();
    }

    void OnTriggerExit(Collider other)
    {
        ulosMeno.clip = ulos[Random.Range(0, sisaan.Count)];
        ulosMeno.Play();
    }
}
