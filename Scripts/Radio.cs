﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Radio : MonoBehaviour, IAction {


    public bool Paalla = false;
    public List<AudioClip> musiikit = new List<AudioClip>();


    private AudioSource audioLahde;
    private int index;
    private bool paused = false;
    // Use this for initialization
    void Start () {
        audioLahde = GetComponent<AudioSource>();
        index = 0;
        audioLahde.clip = musiikit[index];
        audioLahde.Stop();
        Paalla = false;
        paused = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (!audioLahde.isPlaying && !paused)
        {
            index++;
            if(index >= musiikit.Count)
            {
                index = 0;
            }
            audioLahde.clip = musiikit[index];
            audioLahde.Play();

        }

	}

    public void doAction()
    {
        if (audioLahde.isPlaying)
        {
            audioLahde.Pause();
            paused = true;
            Paalla = false;

        }else
        {
            audioLahde.UnPause();
            paused = false;
            Paalla = true;

        }
    }
}
