﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

    public GameObject pauseMenu;
    public GameObject gameInfo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            Time.timeScale = 0;
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            pauseMenu.SetActive(true);
            gameInfo.SetActive(false);
            if (!Cursor.visible)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
            }
            this.enabled = false;
        }
	}
}
