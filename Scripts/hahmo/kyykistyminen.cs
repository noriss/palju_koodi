﻿using UnityEngine;
using System.Collections;

public class kyykistyminen : MonoBehaviour {

    public GameObject kamera;
    public GameObject kohta;

    private Vector3 normalPosition;
	// Use this for initialization
	void Start () {
        normalPosition = kamera.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            kamera.transform.position = kohta.transform.position;
        }else
        {
            kamera.transform.localPosition = normalPosition;
        }
	}
}
