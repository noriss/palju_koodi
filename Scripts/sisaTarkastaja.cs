﻿using UnityEngine;
using System.Collections;

public class sisaTarkastaja : MonoBehaviour
{
    private float waitTime = 1f;
    private float lastTime;

    private bool onkoSisällä = false;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && Time.time - lastTime >= waitTime)
        {
            onkoSisällä = !onkoSisällä;
            other.GetComponent<ambientSound>().siirto(onkoSisällä);
            lastTime = Time.time;
        }
    }
}
