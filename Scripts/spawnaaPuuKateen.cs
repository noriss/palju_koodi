﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spawnaaPuuKateen : MonoBehaviour, IAction {

    public GameObject halko;
    public GameObject pelaaja;
    public List<AudioClip> aanet = new List<AudioClip>();

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void doAction()
    {
        GameObject kopio = Instantiate(halko) as GameObject;
        kopio.transform.position = transform.position;
        pelaaja.GetComponent<PickupObject>().pickupItem(kopio);
        GetComponent<AudioSource>().clip = aanet[Random.Range(0, aanet.Count)];
        GetComponent<AudioSource>().Play();
    }
}
