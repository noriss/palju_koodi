﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class peliTarkistus : MonoBehaviour {

    public bool palju;
    public bool saunaPaalla;
    public GameObject endPanel;
    public GameObject uiPanel;
    public GameObject highScorePanel;
    public GameObject nimi;
    public GameObject uiPaneeliParent;
    public GameObject pelaajaPauseMenu;

    public int pisteet;

    private bool saatu = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(palju && saunaPaalla && !saatu)
        {
            GameObject pelaaja = GameObject.FindGameObjectWithTag("Player");
            pisteet = pelaaja.GetComponent<Pisteet>().getScore();
            pelaaja.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            pelaaja.GetComponent<PickupObject>().enabled = false;
            uiPaneeliParent.GetComponent<PausemenuManager>().enabled = false;
            pelaajaPauseMenu.GetComponent<Pause>().enabled = false;
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            
            endPanel.SetActive(true);
            uiPanel.SetActive(false);
            saatu = true;
        }
	}

    public void enterName()
    {
        string score = nimi.GetComponent<InputField>().text;
        Scores piste = new Scores();
        piste.name = score;
        piste.score = GameObject.FindGameObjectWithTag("Player").GetComponent<Pisteet>().getScore();
        HighScore._instance.SaveHighScore(piste.name, piste.score);
        endPanel.SetActive(false);
        highScorePanel.SetActive(true);
    }

    public void menu()
    {
        Scene scene = SceneManager.GetActiveScene();
        Time.timeScale = 1;
        SceneManager.LoadScene(scene.buildIndex);
    }
}
