﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections;

public class OptionsManager : MonoBehaviour {
    public GameObject volumeSlider;
    public AudioMixerGroup mixer;


	// Use this for initialization
	void Start () {

        float volume;

        mixer.audioMixer.GetFloat("Volume", out volume);

        volumeSlider.GetComponent<Slider>().value = volume;
        
	}
	

    public void volumeChange(float volume)
    {
        mixer.audioMixer.SetFloat("Volume", volume);
    }
}
