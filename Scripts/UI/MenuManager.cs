﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public GameObject HowToPlay;
    public GameObject HighScore;
    public GameObject menu;
    public GameObject options;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void backFromHowto()
    {
        menu.SetActive(true);
        HowToPlay.SetActive(false);
    }

    public void backFromHighScore()
    {
        menu.SetActive(true);
        HighScore.SetActive(false);
    }

    public void backFromOptions()
    {
        menu.SetActive(true);
        options.SetActive(false);
    }
}
