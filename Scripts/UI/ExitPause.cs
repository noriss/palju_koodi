﻿using UnityEngine;
using System.Collections;

public class ExitPause : MonoBehaviour {

    public GameObject ui;
    public GameObject aika;
    private float lastTime;
	// Use this for initialization
	void Start () {
        lastTime = aika.GetComponent<getTime>().getTimef();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.Escape) && Time.timeScale <= 0 && (Time.time - lastTime >= 1f))
        {
            ui.GetComponent<PausemenuManager>().backToGame();
            lastTime = Time.time;
        }
	}
}
