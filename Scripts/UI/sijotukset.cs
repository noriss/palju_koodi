﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class sijotukset : MonoBehaviour {

    public GameObject[] sijat;

	// Use this for initialization
	void Start () {
        string teksti = "";
        List<Scores> pisteet = HighScore._instance.GetHighScore();

        for(int i = 0; i < sijat.Length; i++)
        {
            teksti = (i + 1) + " " + pisteet[i].name + " " + pisteet[i].score;
            sijat[i].GetComponent<Text>().text = teksti;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
