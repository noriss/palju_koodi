﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PausemenuManager : MonoBehaviour {
    public GameObject howtoPlay;
    public GameObject pisteLista;
    public GameObject pauseMenu;
    public GameObject gameInfo;
    public GameObject option;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void Option()
    {
        option.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void backFromOption()
    {
        option.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void howtoPlayButton()
    {
        howtoPlay.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void backFromHowto()
    {
        howtoPlay.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void HighScore()
    {
        pisteLista.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void backFromHighScore()
    {
        pisteLista.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void backToGame()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<Pause>().enabled = true;
        pauseMenu.SetActive(false);
        gameInfo.SetActive(true);
        Time.timeScale = 1;
    }

    public void exitToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
