﻿using UnityEngine;
using System.Collections;

public interface IScore
{
    int getPoints();
}

