﻿using UnityEngine;
using System.Collections;
using System;

public class Suihku : MonoBehaviour, IAction {

    public GameObject suiku;
    public AudioClip paalle, aaniPaalla, kiinni;

    private bool paalla = false;

    
    // Use this for initialization
    void Start () {
        if(GetComponent<AudioSource>() == null)
        {
            gameObject.AddComponent<AudioSource>();
        }

        
    }
	
	// Update is called once per frame
	void Update () {
	    
	}



    public void doAction()
    {

        Debug.Log(paalla);

        if (paalla)
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().clip = paalle;
            GetComponent<AudioSource>().Play();
            StartCoroutine(suihkuPaalle());
            suiku.SetActive(true);

            paalla = !paalla;
        }
        else
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().loop = false;
            GetComponent<AudioSource>().clip = kiinni;
            GetComponent<AudioSource>().Play();
            suiku.SetActive(false);
            paalla = !paalla;
        }

        
    }

    IEnumerator suihkuPaalle()
    {
        yield return new WaitForSeconds(1.5f);
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = aaniPaalla;
        GetComponent<AudioSource>().loop = true;
        GetComponent<AudioSource>().Play();

    }
}
