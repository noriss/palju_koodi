﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour {

    public float maxZoom;
    public float zoomTime;
    public KeyCode zoomNappi;

    float velocity;
    float bakVelocity;
    private float mainFov;
    private Camera playerCamera;
	// Use this for initialization
	void Start () {
        playerCamera = GetComponentInChildren<Camera>();
        mainFov = playerCamera.fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(zoomNappi))
        {
            playerCamera.fieldOfView = Mathf.SmoothDamp(playerCamera.fieldOfView, maxZoom, ref velocity, zoomTime);
        }else
        {

            playerCamera.fieldOfView = Mathf.SmoothDamp(playerCamera.fieldOfView, mainFov, ref bakVelocity, zoomTime);
        }
        
	}
}
